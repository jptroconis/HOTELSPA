export class Config {
    
    public static API_HOTEL='http://127.0.0.1:8080/hotel';
    public static JSON='json';
    public static ICONO_STAR  = './assets/icons/filters/star.svg';
    public static ICONO_SEARCH = './assets/icons/filters/search.svg';
 }