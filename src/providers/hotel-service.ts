import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../const/config';

/*
  Generated class for the HotelServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HotelServiceProvider {

  private path = Config.API_HOTEL;

  private methods = {
    listar : 'listar'
  }

  constructor(private http: HttpClient) {}

  listar( data : any  ){
    return this.http.get(`${this.path}/${this.methods.listar}?${Config.JSON}=${JSON.stringify(data)}`);
  }
/*
  listar( data : any  ){
    return this.http.post(this.path, { 'json': JSON.stringify(data) } );
  }*/

}