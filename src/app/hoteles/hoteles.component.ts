import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-hoteles',
    templateUrl: './hoteles.component.html',
    styleUrls: ['./hoteles.component.scss']
})

export class HotelesComponent {
    
    @Input('hoteles') hoteles : any;
    public moneda : String;
    public etiqueta : String;
    public boton : String;

    constructor() { 
        this.moneda = "ARS";
        this.etiqueta = "Precio por noche por habitacion";
        this.boton = "Ver hotel";
    }

    getStarts(numero){
        let array = [];
        for(let i = 0; i< numero; i++){
          array.push('./assets/icons/filters/star.svg');
        }
        return array;
      }
    
}