import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { isNumber } from 'util';
import { HotelServiceProvider } from '../../providers/hotel-service';
import { element } from 'protractor';
import { Config } from '../../const/config';


export class Data {
    name: String;
    estrellas : any;
}

export class Estrella {
    id : String;
    value : Boolean;
    stars : any;
    isNumber : Boolean;
}

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {

    @Output() salida = new EventEmitter();
    public filtroExtrella = false;
    public estrellas : Estrella[];
    public filtros : String;
    public nombreHotel : String;
    public estrellaLabel : String;
    public aceptar : String;
    public all : String;
    public iconoStar : String;
    public iconoSearch : String;

    public data:Data = {
        name : "",
        estrellas : []
    };

    constructor(private hotelService : HotelServiceProvider) {
        this.iconoStar = Config.ICONO_STAR;
        this.iconoSearch = Config.ICONO_SEARCH;
        this.filtros = "Filtros";
        this.nombreHotel = "Nombre del hotel";
        this.estrellaLabel = "Estrellas"; 
        this.aceptar = "Aceptar";
        this.all = "Todas las estrellas";
        this.estrellas = [
            {
                id : "e0",
                value : true,
                stars : this.all,
                isNumber : false,
            },
            {
                id : "e1",
                value : false,
                stars : 5,
                isNumber : true
            },
            {
                id : "e2",
                value : false,
                stars : 4,
                isNumber : true
            },
            {
                id : "e3",
                value : false,
                stars : 3,
                isNumber : true
            },
            {
                id : "e4",
                value : false,
                stars : 2,
                isNumber : true
            },
            {
                id : "e5",
                value : false,
                stars : 1,
                isNumber : true
            }
        ]
    }

    ngOnInit(){
        this.filtrar();
    }

    public controlSelector(item) : void {
        let itemOne = this.estrellas[0];
        if(itemOne == item){
            for(let i=0; i<this.estrellas.length; i++){
                this.estrellas[i].value = false;
            }
            itemOne.value = true;
        }else if(item.value) {
            itemOne.value = false;
        }
        this.filtrar();
    }

    public filtrar() : void {
        this.data.estrellas = [];
        this.estrellas.forEach(element => {
           if(element.value && element.stars !== this.all){
                this.data.estrellas.push(element.stars);
           }
        });
        this.hotelService.listar({
            name : this.data.name,
            estrellas : this.data.estrellas
        }).subscribe(
            (res) => {
                this.salida.emit(res);
            },
            (error) => {
                 alert(JSON.stringify(error));
            }
        )    
    }

    public getStarts(numero) : any {
        if(isNumber(numero)){
            let array = [];
            for(let i = 0; i< numero; i++){
                array.push(this.iconoStar);
            }
            return array;
        }else{
            return numero;
        }
        
    }
}