# HotelSPA

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

# Instrucciones de ejecución

Accerder desde el terminal y ejecutar el comando 
  `> npm install`

## Luego escriba los siguientes comandos para ejecutar la aplicacion
 `> npm run build`
 `> npm run start` 

[NOTAS] : La instrucción `npm run build` genera la carpeta `/dist` que contiene la aplicación minificada y ofuscada, lista para implantarla en un ambiente productivo.

Accede a la siguiente ruta http://localhost:4200/

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
